Rails.application.routes.draw do
  resources :tokens, only: :create
  resources :available_rooms, only: :index
  post "rooms/:room_id/reservations", to: "reservations#create"
end
