require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "it is invalid without a username" do
    user = User.new
    assert_not user.valid?
  end

  test "it can be saved with a username" do
    user = User.new(username: "Jane123")
    assert user.save
  end

  test "it can have rooms" do
    user = User.create(username: "Jane123")
    room = Room.create(name: "Room 1", user: user)
    assert_equal user.rooms.first, room
  end
end
