require "test_helper"

class RoomTest < ActiveSupport::TestCase
  test "it is invalid without a name" do
    room = Room.new
    assert_not room.valid?
  end

  test "it can be saved with a name" do
    room = Room.new(name: "Room 15")
    assert room.save
  end

  test "it can be related to a user via booking" do
    user = User.create(username: "Susan123")
    room = Room.create(name: "Room 1")
    Booking.create(user: user, room: room)
    assert_equal room.users.to_a, [user]
  end

  test "it validates the amount of bookings" do
    user = User.create(username: "Jane123")
    room = Room.create(name: "Room 1")
    6.times do
      room.bookings << Booking.new(user: user)
    end
    assert_not room.valid?
  end

  test ".available can be used to query available rooms" do
    room1 = Room.create(name: "Room 1")
    5.times do |i|
      user = User.create(username: "Jane#{i}")
      Booking.create(user: user, room: room1)
    end
    room2 = Room.create(name: "Room 2")
    query_result = Room.available
    assert_equal query_result.to_a, [room2]
  end

  test "it autosaves the bookings" do
    user = User.create(username: "JaneDoe")
    room = Room.create(name: "Room 1")
    room.bookings << Booking.new(user: user)
    room.save
    assert room.bookings.first.persisted?
  end

  test "it serializes available spots as json" do
    user = User.create(username: "JaneDoe")
    room = Room.create(name: "Room 1")
    room.bookings << Booking.new(user: user)
    room.save
    assert room.as_json["available_spots"] == 4
  end
end
