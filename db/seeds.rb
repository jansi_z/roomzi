# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def seed_users
  5.times do |i|
    User.create(username: "user#{i}")
  end
end

def seed_full_room
  room = Room.create(name: "I am full")
  User.all.each do |user|
    Booking.create(
      user: user,
      room: room
    )
  end
end

def seed_available_rooms
  5.times do |i|
    Room.create(name: "Room #{i}")
  end
end

puts "Seeding users"
seed_users
puts "Seeding full room"
seed_full_room
puts "Seeding available rooms"
seed_available_rooms
