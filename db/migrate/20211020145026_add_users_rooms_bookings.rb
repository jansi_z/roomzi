class AddUsersRoomsBookings < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :username, null: false
      t.timestamps
    end

    create_table :rooms do |t|
      t.string :name, null: false
      t.references :user
      t.timestamps
    end
  end
end
