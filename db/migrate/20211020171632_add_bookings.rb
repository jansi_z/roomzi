class AddBookings < ActiveRecord::Migration[6.1]
  def change
    remove_column :rooms, :user_id
    add_column :rooms, :user_count, :integer

    create_table :bookings do |t|
      t.belongs_to :room
      t.belongs_to :user
      t.timestamps
    end
  end
end
