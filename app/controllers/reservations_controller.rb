class ReservationsController < ApplicationController
  def create
    room = Room.find(params[:room_id])
    room.bookings << Booking.new(user: current_user)

    if room.save
      render json: { message: "Reservation successful", room: room }
    else
      render json: { error: room.errors.full_messages }, status: :unprocessable_entity
    end
  end
end
