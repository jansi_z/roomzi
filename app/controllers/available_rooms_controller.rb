class AvailableRoomsController < ApplicationController
  def index
    render json: Room.available
  end
end
