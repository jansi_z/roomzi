module MessageVerification
  extend ActiveSupport::Concern

  # TODO: use a better secret + store it in a better place
  SECRET = "s3cr4t".freeze

  def token_verifier
    ActiveSupport::MessageVerifier.new(SECRET)
  end

  def generate_token(username)
    token_verifier.generate(username, purpose: :authentication)
  end

  def verify_username(token)
    token_verifier.verify(token, purpose: :authentication)
  rescue ActiveSupport::MessageVerifier::InvalidSignature
    nil
  end
end
