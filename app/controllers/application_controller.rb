class ApplicationController < ActionController::API
  include MessageVerification

  before_action :verify_token

  private

  def verify_token
    return missing_token if params[:token].blank?

    username = verify_username(params[:token])

    return invalid_token if username.blank?

    # TODO: when a formerly existing user is deleted, the token
    # still checks out but @current_user will be nil.
    @current_user = User.find_by(username: username)
  end

  def current_user
    @current_user
  end

  def missing_token
    render json: { error: "Missing token" }, status: :unauthorized
  end

  def invalid_token
    render json: { error: "Invalid token" }, status: :unauthorized
  end
end
