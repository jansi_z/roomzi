class TokensController < ApplicationController
  skip_before_action :verify_token

  def create
    user = User.find_or_create_by(username: token_params[:username])
    token = generate_token(user.username)
    render json: { token: token }
  end

  private

  def token_params
    params.require(:token).permit(:username)
  end
end
