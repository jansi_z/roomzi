class Room < ApplicationRecord
  validates :name, presence: true
  validate :validate_bookings
  has_many :bookings, autosave: true
  has_many :users, through: :bookings

  scope :available, -> { where("user_count IS NULL OR user_count < ?", 5) }

  def as_json(args)
    attrs = super(args)
    attrs["available_spots"] = 5 - bookings.count
    attrs
  end

  private

  def validate_bookings
    errors.add(:bookings, "too many") if bookings.count > 5
  end
end
