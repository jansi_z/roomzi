class Booking < ApplicationRecord
  belongs_to :user
  belongs_to :room, counter_cache: :user_count
end
