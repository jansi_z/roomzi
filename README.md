# ROOMZI - BOOKING ROOMS MADE EASY

This application implements a booking system for rooms by users. It consists of a simple API service built
with Ruby on Rails. The app supports two main features:

- Users can view available rooms
- Users can make a reservation for a room if it has available spots

## INSTALLATION

The app uses bundler, so all you need to do is clone the repo and run `bundler` in the directory. After
that, run `rails s` to start the app.

## USING THE APP

To view and book rooms, you first need to register as a user and generate a token. Simply
post to the /tokens endpoint and supply a username:

`curl --header "Content-Type: application/json" -d "{\"username\":\"jansi\"}" http://localhost:3000/tokens`

Using the token, it is possible to view available rooms:

`curl http://localhost:3000/available_rooms?token=[your_token]`

Making a reservation is done as follows:

`curl -X POST http://localhost:3000/rooms/[room_id]/reservations?token=[your_token]`

## TESTS

Run `rails test` to run the test suite. Unfortunately, two tests are currently failing because
of a last-minute change I made.

## APPROACH

### Framework

There are a number of lightweight frameworks available to quickly get a webserver up and running. 
And there is always Rails, which I am most familiar with. I narrowed my options down to these two:

- Grape: a simple API framework that I have worked with before
- Rails: in its standard configuration a complex behemoth, but one I feel very comfortable with

I then considered the requirements. Because users need to make bookings, some manner of persistance felt
necessary. That means I would need a ORM library to store entries in a database. I considered the option
of using Grape with a library like Sequel because it would be more lightweight and simple; but in the end,
I decided to use Rails after all. The reason is that I wanted to be as productive as possible. So I choose
the familiar, to prevent myself from getting stranded in the [unknown unknowns](https://en.wikipedia.org/wiki/There_are_known_knowns)
of a framework or library that uses an unfamiliar DSL. I did get rid of most of the `rails new` bloat though.

### Defining models (1h)

I started out with the models and wrote tests for them. I took 1 hour for that. In hindsight,
I was a little too ambitious here... I visualised a fairly complex booking system that included time slots. 
When the hour was full, I had a system of Users, Rooms, Bookings, and a TimeSlot-class that could be
used to check whether a room was booked at that time. Looking back, I overstretched here, because the
time slots were not in the requirements but they created issues down the road.

### Routes, resources, 'authentication' (1h)

I spent about 45 minutes setting up a simple way of creating a user and returning a token. Then I started
working on the available rooms endpoint. At this point, I hit a bump: I discovered the booking model I had
was too complex. I had roughly 1 hour left, and I wouldn't be able to fit both the available rooms plus
the option to book rooms (and show available spots) in that time. I decided to take a 15 minute break to clear 
my head before getting back to work.

### Simplifying things, putting it all together (1h)

I got rid of the complex time slot logic, and returned to a simple User -> Room association that I later
upgraded to User -> Booking -> Room. I first created the endpoints for booking reservations and viewing
available rooms, and then I added in the constraint of 5 spots availibility. I had an alarm set 3 hours
after I started, but when it went off, a couple of things weren't working. I decided to clean that
up and make my final commit. That means I ran about 5 minutes over time.
